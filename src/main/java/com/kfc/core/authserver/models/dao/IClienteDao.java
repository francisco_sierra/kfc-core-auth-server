package com.kfc.core.authserver.models.dao;

import java.util.List;

import com.kfc.core.authserver.models.entity.Cliente;
import com.kfc.core.authserver.models.entity.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface IClienteDao extends JpaRepository<Cliente, Long>{

	@Query("from Region")
	public List<Region> findAllRegiones();
}
