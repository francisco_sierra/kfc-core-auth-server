package com.kfc.core.authserver.models.services;

import com.kfc.core.authserver.models.entity.Usuario;

public interface IUsuarioService {

	public Usuario findByUsername(String username);
}
