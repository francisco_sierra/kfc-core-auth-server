package com.kfc.core.authserver.models.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
public class OauthClientDetails {

  @Id
  private String clientId;
  private String resourceIds;
  private String clientSecret;
  private String scope;
  private String authorizedGrantTypes;
  private String webServerRedirectUri;
  private String authorities;
  private long accessTokenValidity;
  private long refreshTokenValidity;
  private String additionalInformation;
  private String autoapprove;
}
