package com.kfc.core.authserver.models.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

@Data
public class OauthClientToken {

  @Id
  private String tokenId;
  @Lob
  @Column(name = "token", columnDefinition="BLOB")
  private String token;
  private String authenticationId;
  private String userName;
  private String clientId;
}
