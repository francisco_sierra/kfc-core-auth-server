package com.kfc.core.authserver.models.entity;

import lombok.Data;

import javax.persistence.Entity;

@Data
public class OauthRefreshToken {

  private String tokenId;
  private String token;
  private String authentication;
}
