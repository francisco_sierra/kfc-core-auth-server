package com.kfc.core.authserver.models.entity;

import lombok.Data;

import javax.persistence.Entity;

@Data
public class OauthApprovals {

  private String userId;
  private String clientId;
  private String scope;
  private String status;
  private java.sql.Timestamp expiresAt;
  private java.sql.Timestamp lastModifiedAt;

}
