package com.kfc.core.authserver.models.entity;

import lombok.Data;

import javax.persistence.Entity;

@Data
public class OauthCode {

  private String code;
  private String authentication;

}
