package com.kfc.core.authserver.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.kfc.core.authserver.models.entity.Factura;

public interface IFacturaDao extends CrudRepository<Factura, Long>{

}
